#include <vector>
#include <string> 
#include <memory>
#include <set>
#include <unitOfIdent.h> 

class Identifier {
    public:
    Identifier();
    void setCurVal(std::vector<std::pair<std::string,int>> pairVec);
    void incriment();
    bool checkElem();
    void printIde();
    private:
   
    std::vector<std::shared_ptr<UnitOfIdent>> ident;
    std::set <std::string> letters{"A","B","C","E","H","I","K","L","N","O","P","R","S","T","U","W","X","Y","Z"};
    std::set <int> vals{1,2,3,4,5,6,7,8,9};

   


};