cmake_minimum_required(VERSION 3.15.0)

message( STATUS "Build type is " ${CMAKE_BUILD_TYPE} )
message(CMAKE_VERSION: ${CMAKE_VERSION})
project( main
        DESCRIPTION     "main"
        VERSION         1.0.0
        LANGUAGES       CXX )



set (SOURCES main.cpp
	     unitOfIdent.cpp
	     identifier.cpp
	     identifier.h
	     unitOfIdent.h	
			    )


set(CMAKE_BUILD_TYPE "RelWithDebInfo")
add_executable(${PROJECT_NAME}
    ${SOURCES} 
)
target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR})

set (_flags_with_dbg_info "-g")

message(!!!!!!!!!!!!!!!!!!!!!${PROJECT_SOURCE_DIR})
set_source_files_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "${_flags_with_dbg_info}")
set_property(TARGET ${PROJECT_NAME} PROPERTY ${_flags_with_dbg_info} CXX_STANDARD 20)

target_sources( ${PROJECT_NAME}
  PRIVATE     ${SOURCES} )


target_compile_options(${PROJECT_NAME} PUBLIC ${_flags_with_dbg_info})

INSTALL(
TARGETS ${PROJECT_NAME} ARCHIVE 
DESTINATION bin 
COMPONENT runtime )


set(CMAKE_CXX_FLAGS_DEBUG_INIT "-O3")


set(CMAKE_INSTALL_PREFIX "/usr")
set(CPACK_GENERATOR "DEB;RPM;TGZ")
set(CPACK_PACKAGE_VERSION ${VERSION_DESCRIBE})

set(CPACK_STRIP_FILES OFF)
set( CPACK_PACKAGE_NAME                ${PROJECT_NAME} )
set( CPACK_PACKAGE_CONTACT             "Contact name" )
set( CPACK_PACKAGE_VERSION             ${PROJECT_VERSION} )
set( CPACK_PACKAGE_DESCRIPTION_SUMMARY "Description" )


set( CPACK_GENERATOR                   DEB )
set( CPACK_DEB_COMPONENT_INSTALL       ON )
set( CPACK_DEBIAN_PACKAGE_DEPENDS      "" )
set( CPACK_DEBIAN_RUNTIME_DEBUGINFO_PACKAGE    ON )






include( CPack )
