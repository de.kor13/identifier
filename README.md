# Identifier Class



## what does the program do?

1. The first sequence ID is "A1", the second is "A2", the third is "A3" and so on. "A9" is followed by "B1". The next after "Z9" is "A1-A1", then "A1-A2" and so on. "A1-Z9" is followed by "A2-A1".
2. The maximum length of an identifier is ten groups of two characters.
3. The letters "D", "F", "G", "J", "M", "Q", "V" and the number "0" should never increase in identifiers.
It is necessary to implement a class, the implementation of working with an identifier in a given order.
The class must have functional functionality:
• A method that sets the initial value of an identifier
• A method that increments the value of an identifier and returns a new value

## build
1. mkdir build && cd build

2. cmake .. -DCMAKE_BUILD_TYPE=Debug 
3. cmake -DCMAKE_CXX_COMPILER:STRING="путь/до/компилятора"
4. make
5. ./main
if Debian 
1. mkdir build && cd build
2. cmake .. -DCMAKE_BUILD_TYPE=Debug -DCPACK_GENERATOR=DEB -DCMAKE_CXX_COMPILER:STRING="путь/до/компилятора" -DCMAKE_PREFIX_PATH=/usr/lib && make package
3. ./main




