#include <iostream>
#include <vector>
#include <string> 
#include <memory>

#include <identifier.h> 
#include <unitOfIdent.h>

int main()
{

   std::shared_ptr<Identifier> Ide = std::make_shared<Identifier>();
   std::vector<std::pair<std::string,int>> pairVec;

    pairVec.push_back(std::make_pair(std::string("Z"),9));  
    Ide->setCurVal(pairVec);
    Ide->printIde();
    Ide->incriment();
    Ide->printIde();

    std::cout<<"////////////////////"<<std::endl;

    pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));   
    Ide->setCurVal(pairVec);  
     Ide->printIde(); 
    Ide->incriment();
    Ide->printIde(); 
    std::cout<<"////////////////////"<<std::endl;
    pairVec.push_back(std::make_pair(std::string("D"),9));
    Ide->setCurVal(pairVec);
     std::cout<<"////////////////////"<<std::endl;
     pairVec.push_back(std::make_pair(std::string("Z"),0)); 
      Ide->setCurVal(pairVec);
      std::cout<<"////////////////////"<<std::endl;
          pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9)); 
              pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9)); 
              pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9)); 
              pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));
    pairVec.push_back(std::make_pair(std::string("Z"),9));   
    Ide->setCurVal(pairVec); 
    pairVec.clear();
    std::cout<<"////////////////////"<<std::endl;
    pairVec.push_back(std::make_pair(std::string("C"),9));
    Ide->setCurVal(pairVec);
    Ide->printIde(); 
    Ide->incriment();
    Ide->printIde(); 

  



return 0;
 
}

